<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt('a'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Project::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->userName,
        'description' => $faker->realText(100),
        'git_link' => $faker->url,
        'budget' => $faker->numberBetween($min = 500, $max = 9000),
        'paid' => $faker->numberBetween($min = 10, $max = 500),
        'project_type' => $faker->randomElement(['freelance','product']),
        'project_status' => $faker->randomElement(['completed','running','rejected']),
        'user_id' => 12
    ];
});

$factory->define(App\Resource::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->userName,
        'description' => $faker->realText(100),
        'file_path' => $faker->url,
        'user_id' => $faker->numberBetween(1,10)
    ];
});

$factory->define(App\Apk::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->userName,
        'description' => $faker->realText(100),
        'file_path' => $faker->url,
        'user_id' => $faker->numberBetween(1,10)
    ];
});

