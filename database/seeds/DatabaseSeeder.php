<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         $this->call(BootSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(PostBootSeeder::class);
         $this->call(ProjectsTableSeeder::class);

        Model::reguard();
    }
}
