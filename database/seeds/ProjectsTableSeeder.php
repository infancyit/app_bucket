<?php

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Project::class, 30)->create()->each(function($u) {
            $u->resources()->save(factory(App\Resource::class)->make());

            if($u->id % 2 === 0){
                $u->apks()->save(factory(App\Apk::class)->make());
            }
        });
    }
}
