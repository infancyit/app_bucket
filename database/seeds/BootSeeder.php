<?php

use Illuminate\Database\Seeder;

class BootSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //loading roles
        $roles = config('cons.roles');
        foreach ($roles as $key => $role){
            DB::table('roles')->insert([
                'name' => $role
            ]);
        }

        //loading abilities
        $admin_abilities = config('cons.admin_abilities');
        foreach ($admin_abilities as $key => $ability){
            DB::table('abilities')->insert([
                'name' => $ability
            ]);
        }

        //assigning ability to roles
        Bouncer::allow(config('cons.roles.admin'))->to(array_flatten(config('cons.admin_abilities')));
    }
}
