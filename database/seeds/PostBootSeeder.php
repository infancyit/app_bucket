<?php

use Illuminate\Database\Seeder;

class PostBootSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //creating user for testing admin role
        $rat = \App\User::create([
            'name' => 'rat',
            'email' => 'ratulcse27@gmail.com',
            'password' => bcrypt('a'),
            'approve_status' => config('cons.user_status.approved'),
            'remember_token' => str_random(10),
        ]);
        $rat->assign(config('cons.roles.admin'));

        //creating user for testing manager role
        $abu = \App\User::create([
            'name' => 'abu',
            'email' => 'ratulcse10@gmail.com',
            'password' => bcrypt('a'),
            'approve_status' => config('cons.user_status.approved'),
            'remember_token' => str_random(10),
        ]);
        $abu->assign(config('cons.roles.manager'));
    }
}
