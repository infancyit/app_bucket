<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Silber\Bouncer\Database\HasRolesAndAbilities;
class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    use HasRolesAndAbilities;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password','approve_status'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token','approve_status'];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function resources()
    {
        return $this->hasMany(Resource::class);
    }

    public function apks()
    {
        return $this->hasMany(Apk::class);
    }

    public function scopePending($query){
        return $query->where('approve_status',config('cons.user_status.pending'));
    }

    public function scopeRejected($query){
        return $query->where('approve_status',config('cons.user_status.rejected'));
    }

    public function scopeApproved($query){
        return $query->where('approve_status',config('cons.user_status.approved'));
    }

    public function scopeBanned($query){
        return $query->where('approve_status',config('cons.user_status.banned'));
    }

}
