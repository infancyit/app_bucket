<?php

namespace App\Http\Middleware;

use Closure;
use Bouncer;
use Dingo\Api\Routing\Helpers;

class RoleMiddleware
{
    use Helpers;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
        if(Bouncer::is($request->user())->a(config('cons.roles.admin'))){
            return $next($request);
        }
        elseif(!Bouncer::is($request->user())->a($role)){
            return $this->response->errorUnauthorized();
        }
        return $next($request);
    }
}
