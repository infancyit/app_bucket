<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->get('test','AuthController@test');
    // Set our namespace for the underlying routes
    $api->group(['namespace' => 'Api\Controllers'], function ($api) {
        $api->get('refresh', 'AuthController@refreshToken');

        // Login route

        $api->post('login', 'AuthController@authenticate');
        $api->post('register', 'AuthController@register');
        $api->get('users/me', 'AuthController@me');

        //Admin route

        $api->get('pending','AdminController@pendingUsers');
        $api->get('approved','AdminController@approvedUsers');
        $api->get('rejected','AdminController@rejectedUsers');
        $api->get('banned','AdminController@bannedUsers');

        $api->get('approve/{user_id}','AdminController@approveUser')->where('id', '[0-9]+');
        $api->get('reject/{user_id}','AdminController@rejectUser')->where('id', '[0-9]+');
        $api->get('ban/{user_id}','AdminController@banUser')->where('id', '[0-9]+');

        $api->get('managers','AdminController@managers');
        $api->get('managers/assign/{user_id}','AdminController@makeManager')->where('id', '[0-9]+');
        $api->get('managers/revoke/{user_id}','AdminController@revokeManager')->where('id', '[0-9]+');

        //Admin route


        //Manager route

        $api->get('projects','ProjectController@index');
        $api->post('projects/create','ProjectController@store');
        $api->get('projects/{id}','ProjectController@show');
        $api->put('projects/{id}','ProjectController@update');
        $api->delete('projects/{id}','ProjectController@delete');

        $api->get('projects/{projects}/resources','ResourceController@index');
        $api->post('projects/{projects}/resources','ResourceController@store');
        $api->get('projects/{projects}/resources/{resources}','ResourceController@show');
        $api->put('projects/{projects}/resources/{resources}','ResourceController@update');
        $api->delete('projects/{projects}/resources/{resources}','ResourceController@delete');

        $api->get('project/{id}/apks','ApkController@index');

        //Manager route


        $api->get('test',function(){

            return dd(is_null(\App\Project::find(1)));
        });
    });
});