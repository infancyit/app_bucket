<?php

namespace App\Policies;

use App\Project;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Bouncer;

class ProjectPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
        if(Bouncer::is($user)->a(config('cons.roles.admin'))){
            return true;
        }

    }

    public function update(User $user, Project $project)
    {
        return $user->id === $project->user_id;
    }

    public function delete(User $user, Project $project)
    {
        return $user->id === $project->user_id;
    }
}
