<?php


namespace Api\Requests;


use App\Project;
use App\Resource;
use Dingo\Api\Http\FormRequest;
use Gate;
class ResourceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $projects = $this->route('projects');
        if(is_null(Project::find($projects))){
            return false;
        }
        return true;

        /*$id = $this->route('resources');

        if (Gate::denies('update', Resource::findOrFail($id))) {
            return false;
        }*/
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'file' => 'required|mimes:jpeg,jpg,bmp,png,7z,txt,rar,pdf|max:50000'
        ];
    }

}