<?php
/**
 * Created by PhpStorm.
 * User: rat
 * Date: 8/8/2016
 * Time: 3:19 AM
 */

namespace Api\Requests;


use App\Project;
use Dingo\Api\Http\FormRequest;
use Gate;
class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id = $this->route('id');
        if (Gate::denies('update', Project::findOrFail($id))) {
            return false;
        }
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'budget' => 'numeric',
            'paid' => 'numeric'
        ];
    }

}