<?php
namespace Api\Transformers;
use App\Apk;
use League\Fractal\TransformerAbstract;
class ApkTransformer extends TransformerAbstract
{

    public function transform(Apk $apk)
    {
        return [
            'id' 	=> (int) $apk->id,
            'title'  => $apk->title,
            'description'	=> $apk->description,
            'file_path'	=> $apk->file_path
        ];
    }
}