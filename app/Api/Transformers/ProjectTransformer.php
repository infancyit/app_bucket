<?php
namespace Api\Transformers;
use App\Project;
use Api\Transformers\ResourceTransformer;
use Api\Transformers\ApkTransformer;
use League\Fractal\TransformerAbstract;
class ProjectTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'resources',
        'apks'
    ];

    public function transform(Project $project)
    {
        return [
            'id' 	=> (int) $project->id,
            'title'  => $project->title,
            'description'	=> $project->description,
            'git_link'	=> $project->git_link,
            'budget'	=> $project->budget,
            'paid'	=> $project->paid,
            'project_type'	=> $project->project_type,
            'project_status'	=> $project->project_status,
        ];
    }

    public function includeResources(Project $project)
    {
        $resources = $project->resources;

        return $this->collection($resources, new ResourceTransformer);
    }

    public function includeApks(Project $project)
    {
        $apks = $project->apks;

        return $this->collection($apks, new ApkTransformer);
    }
}