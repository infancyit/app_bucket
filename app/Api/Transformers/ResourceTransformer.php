<?php
namespace Api\Transformers;
use App\Resource as ProjectResource;
use League\Fractal\TransformerAbstract;
class ResourceTransformer extends TransformerAbstract
{
    public function transform(ProjectResource $resource)
    {
        return [
            'id' 	=> (int) $resource->id,
            'title'  => $resource->title,
            'description'	=> $resource->description,
            'file_path'	=> $resource->file_path
        ];
    }
}