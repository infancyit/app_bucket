<?php
namespace Api\Controllers;
use Api\Transformers\UserTransformer;
use App\User;
use Illuminate\Http\Request;
use Api\Requests\UserRequest;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
class AuthController extends BaseController
{
    public function __construct()
    {
        $this->middleware('jwt.auth',['only' => ['me']]);

    }

    public function me(Request $request)
    {
        $user =  JWTAuth::parseToken()->authenticate();
        return $this->response->item($user,UserTransformer::class);
    }
    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return $this->response->errorUnauthorized();
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return $this->response->errorInternal();
        }
        // all good so return the token
        return response()->json(compact('token'));
    }

    public function register(UserRequest $request)
    {
        $user = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ];
        try{
            if($newUser = User::create($user)){
                return $this->response->item($newUser,UserTransformer::class)
                    ->addMeta('message',"Hi $newUser->name"." your registration is completed.You will get a confirmation Email after approval.")
                    ->addMeta('status_code',200);
            }else{
                return $this->response->errorInternal();
            }
        }catch (\Exception $ex){
            return $this->response->errorInternal();
        }


    }

    public function refreshToken(){
        $token = JWTAuth::getToken();
        if(!$token){
            return $this->response->errorBadRequest('Token not provided');
        }
        try{
            $token = JWTAuth::refresh($token);
        }catch(\Exception $ex){
            return $this->response->errorForbidden('The token is invalid');
        }
        return $this->response->withArray(['token'=>$token]);
    }
}