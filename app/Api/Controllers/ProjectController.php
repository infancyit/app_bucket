<?php
namespace Api\Controllers;
use Api\Requests\ProjectRequest;
use Api\Transformers\ProjectTransformer;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Gate;
class ProjectController extends BaseController
{

    public function __construct()
    {
        $this->middleware('jwt.auth');

        $this->middleware('role:Manager', ['except' => ['show']]);

    }

    public function index(){

        $project = Project::paginate(10);
        return $this->response->paginator($project,ProjectTransformer::class);

    }

    public function store(ProjectRequest $request){

        $user = JWTAuth::parseToken()->authenticate();

        $project = [
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'git_link' => $request->get('git_link'),
            'budget' => $request->get('budget'),
            'paid' => $request->get('paid'),
            'project_type' => $request->get('project_type'),
            'project_status' => $request->get('project_status'),
            'user_id' => $user->id
        ];

        try{
            if($newProject = Project::create($project)){
                return $this->response->item($newProject,ProjectTransformer::class)->addMeta('status_code',200);
            }else{
                return $this->response->errorInternal();
            }

        }catch (\Exception $ex){
            return $this->response->errorInternal();
        }


    }

    public function show($id){
        try{
            $project = Project::findOrFail($id);
            return $this->response->item($project,ProjectTransformer::class);
        }catch (\Exception $ex){
            return $this->response->errorNotFound("Project not Found");
        }
    }

    public function update(ProjectRequest $request, $id){

        $project = Project::findOrFail($id);
        //dd($project->update($request->all()));

        try{
            if($project->update($request->all())){
                return $this->response->item($project,ProjectTransformer::class)->addMeta('status_code',200);
            }else{
                return $this->response->errorInternal();
            }

        }catch (\Exception $ex){
            return $this->response->errorInternal();
        }



    }

    public function delete($id){
        $this->authorize(Project::findOrFail($id));

        try {
            if(Project::destroy($id)){
                return response()->json([
                    'message' => 'Project Deleted successfully.',
                ]);
            }else{
                $this->response()->errorNotFound('Project not found');
            }
        } catch (Exception $e) {
            $this->response()->errorInternal();
        }

    }

}