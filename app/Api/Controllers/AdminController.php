<?php
namespace Api\Controllers;
use Illuminate\Http\Request;
use App\User;
use Api\Transformers\UserTransformer;
use Bouncer;
class AdminController extends BaseController
{

    public function __construct()
    {
        $this->middleware('jwt.auth');

        $this->middleware('role:Admin', ['except' => []]);

    }

    /*
     * get all pending users
     * */
    public function pendingUsers(){
        $users = User::pending()->paginate(10);
        return $this->response->paginator($users, new UserTransformer);
    }

    /*
     * get all approved users
     * */
    public function approvedUsers(){
        $users = User::approved()->paginate(10);
        return $this->response->paginator($users, new UserTransformer);
    }

    /*
     * get all rejected users
     * */
    public function rejectedUsers(){
        $users = User::rejected()->paginate(10);
        return $this->response->paginator($users, new UserTransformer);
    }

    /*
     * get all banned users
     * */
    public function bannedUsers(){
        $users = User::banned()->paginate(10);
        return $this->response->paginator($users, new UserTransformer);
    }

    public function approveUser($user_id){
        try {
            $user = User::findOrFail($user_id);
            if($user->update(['approve_status' => config('cons.user_status.approved')])){
                return \Response::json([
                    'message' => 'User Approved',
                    'user' => $user,
                    'status_code' => 200
                ],200);
            }else{
                return $this->response->errorInternal("Something went wrong.Please Try again.");
            }

        } catch (\Exception $e) {
            return $this->response->errorNotFound('User Not Found');
        }
    }

    public function rejectUser($user_id){
        try {
            $user = User::findOrFail($user_id);
            if($user->update(['approve_status' => config('cons.user_status.rejected')])){
                return \Response::json([
                    'message' => 'User Rejected',
                    'user' => $user,
                    'status_code' => 200
                ],200);
            }else{
                return $this->response->errorInternal("Something went wrong.Please Try again.");
            }

        } catch (\Exception $e) {
            return $this->response->errorNotFound('User Not Found');
        }
    }

    public function banUser($user_id){
        //return config('cons.user_status.banned');
        try {
            $user = User::findOrFail($user_id);
            if($user->update(['approve_status' => config('cons.user_status.banned')])){
                return \Response::json([
                    'message' => 'User Banned',
                    'user' => $user,
                    'status_code' => 200
                ],200);
            }else{
                return $this->response->errorInternal("Something went wrong.Please Try again.");
            }

        } catch (\Exception $e) {
            return $this->response->errorNotFound('User Not Found');
        }
    }

    public function makeManager($user_id){
        $user = User::findOrFail($user_id);
        Bouncer::assign(config('cons.roles.manager'))->to($user);
        return response()->json([
            'message' => "$user->name". " is now Manager"
        ],200);
    }

    public function revokeManager($user_id){
        $user = User::findOrFail($user_id);
        Bouncer::retract(config('cons.roles.manager'))->from($user);
        return response()->json([
            'message' => "$user->name". " Manager role is Revoked."
        ],200);
    }

    public function managers(){

        $users_id =  \DB::table('user_roles')->where('role_id',2)->lists('user_id');
        if(!empty($users_id)){
            $users = User::whereIn('id',$users_id)->paginate(10);
            return $this->response->paginator($users, new UserTransformer);
        }else{
            return response()->json([
                'message' => "Currently there is no Manager."
            ],200);
        }
    }
}