<?php
namespace Api\Controllers;
use Api\Requests\ResourceRequest;
use Api\Transformers\ResourceTransformer;
use App\Resource;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
class ResourceController extends BaseController
{

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function index($projects){
        $resource = Resource::where('project_id',$projects)->paginate(10);
        return $this->response->paginator($resource,ResourceTransformer::class);
    }

    public function store(ResourceRequest $request,$projects){

        $user = JWTAuth::parseToken()->authenticate();

        $file = $request->file('file');

        $destination = public_path().'/uploads/files/';
        $filename = time().'_'.$file->getClientOriginalName();
        if($file->move($destination, $filename)){
            $file_url = $destination.$filename;
        }else{
            return $this->response->errorInternal("File can not be saved. Try again.");
        }

        $resource = [
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'file_path' => $file_url,
            'project_id' => $projects,
            'user_id' => $user->id

        ];

        try{
            if($newResource = Resource::create($resource)){
                return $this->response->item($newResource,ResourceTransformer::class)->addMeta('status_code',200);
            }else{
                return $this->response->errorInternal('first');
            }

        }catch (\Exception $ex){
            return $this->response->errorInternal('sec');
        }
    }

}