<?php
namespace Api\Controllers;
use Api\Transformers\ApkTransformer;
use App\Apk;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
class ApkController extends BaseController
{

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function index($id){
        $apk = Apk::where('project_id',$id)->paginate(10);
        return $this->response->paginator($apk,ApkTransformer::class);
    }
}