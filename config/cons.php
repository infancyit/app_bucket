<?php

return [
    'roles' => [
        'admin' => 'Admin',
        'manager' => 'Manager',
        'user' => 'User'
    ],
    'admin_abilities' => [
        'assign-role' => 'can-assign-role',
        'remove-user' => 'can-remove-user',
        'approve-user' => 'can-approve-user'
    ],
    'user_status' => [
        'approved' => 'approved',
        'pending' => 'pending',
        'rejected' => 'rejected',
        'banned'   => 'banned'
    ],

];